<?php include 'includes/header.php';?>

<div id="rotateScreen" class="active">
    <span class="icon"></span>
    <h1>This is best <br /> viewed in portrait</h1>
    <h2>Please rotate your display</h2>
</div>

<header>
    <div>
        <img src="images/img-lift-airline-wt.jpg" alt="Lift Airline" title="Lift Airline" />
    </div>
</header>

<section id="content-main-section">
    <div class="container">
        <p>
            We’re going UP! <br />
            It’s time for a new kind of airline. Coming December 2020. <br />
            Sign up now to be the first to hear when we LIFT off
        </p>
    </div>
</section>

<section id="contact-us-section" class="scroll-animations row no-margin">
    <div class="container">
        <div id="contact_form">
            <form id="contact-us" name="frmContact" method="post" action="" enctype="multipart/form-data"
            onsubmit="return validateContactForm()">
                <div class="row no-margin">
                    <div class="float-left col-xs-12 xol-sm-12 col-md-6 col-lg-6">
                        <input id="firstName" class="input-field" type="text" name="firstName" placeholder="First Name" />
                        <span id="firstName-info" class="info"></span>
                    </div>
                    <div class="float-right col-xs-12 xol-sm-12 col-md-6 col-lg-6">
                        <input id="lastName" class="input-field" type="text" name="lastName" placeholder="Last Name" />   
                        <span id="lastName-info" class="info"></span> 
                    </div>   
                </div> 
                <input id="subject" class="input-field" type="hidden" name="subject" value="New Enquiry" /> 
                <div class="clearfix"></div>
                <div class="row no-margin">
                    <input id="userEmail" class="input-field" type="email" name="userEmail" placeholder="Email Address" /> 
                    <span id="userEmail-info" class="info"></span>   
                </div>
                 <!--<div class="row no-margin">
                    <input id="telNumber" class="input-field" type="tel" name="telNumber" placeholder="Telephone Number" /> 
                    <span id="telNumber-info" class="info"></span>   
                </div>
               <div class="row no-margin">
                    <textarea id="message" class="input-field" type="text" name="message" placeholder="Message"></textarea>   
                    <span id="userMessage-info" class="info"></span>
                    <span id="charCount" class="char-count float-right">1000 characters left</span>
                </div>-->
                <input id="submit-contact-form" class="btn btn-secondary button" type="submit" name="send" value="Submit" />                
            </form>
        </div>
    </div>
</section>

<!-- Contact form Success Modal -->
<div class="modal fade" id="thankyouModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header"><h2>We have received your message</h2></div>
            <div class="modal-body">
                <p>Thank you, your contact information and message has been received successfully.</p>
            </div>
            <div class="modal-footer">
                <button id="resetForm" class="btn btn-secondary button" type="button" class="close" data-dismiss="modal" aria-label="Close">Dismiss</button>
            </div>
        </div>
    </div>
</div>
<?php include 'includes/footer.php';?>
