$(window).on('load', function() {
    if ($.cookie('pop') == null) {
        $('#VideoModal').modal('show');
        $.cookie('pop', '7');
    }
});

$(window).on('load', function() {
    if (window.location.href.indexOf('#thankyou') != -1) {
        $('#thankyouModal').modal('show');
    }
});

$(document).ready(function() {
    var $slider = $('.slider');
    var $progressBar = $('.progress');
    var $progressBarLabel = $('.slider__label');

    $progressBar
            .css('background-size', '16.666666666666664% 100%')
            .attr('aria-valuenow', 0);

    $slider.on('beforeChange', function(event, slick, currentSlide, nextSlide) {
        var calc = ((nextSlide + 1) / (slick.slideCount)) * 100;

        $progressBar
            .css('background-size', calc + '% 100%')
            .attr('aria-valuenow', calc);

        $progressBarLabel.text(calc + '% completed');

        console.log(calc + '% completed');
    });

    $('.hero-banner-slider').on('init', function(e, slick) {
        var $firstAnimatingElements = $('div.hero-slide:first-child').find('[data-animation]');
        doAnimations($firstAnimatingElements);
    });
    $('.hero-banner-slider').on('beforeChange', function(e, slick, currentSlide, nextSlide) {
        var $animatingElements = $('div.hero-slide[data-slick-index="' + nextSlide + '"]').find('[data-animation]');
        doAnimations($animatingElements);
    });

    $slider.slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        fade: true,
        speed: 800,
        autoplay: true,
        dots: true,
        mobileFirst: true,
        arrows: false,
        touchMove: false,
        draggable: false,
        pauseOnHover: false,
        pauseOnDotsHover: false,
        customPaging: function(slider, i) {
            var title = $(slider.$slides[i]).find('[data-title]').data('title');
            return '<a class="pager__item"> ' + title + ' </a>';
        },
    });

    //RESET FORM AND REMOVE HASH URL

    $("#resetForm").click(function() {
        $("#contact-us")[0].reset()
        history.replaceState(null, null, ' ');
    });

    var text_max = 1000;
    $('#charCount').html(text_max + ' characters remaining');

    $('#message').keyup(function() {
        var text_length = $('#message').val().length;
        var text_remaining = text_max - text_length;

        $('#charCount').html(text_remaining + ' characters remaining');
    });


    function doAnimations(elements) {
        var animationEndEvents = 'webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend';
        elements.each(function() {
            var $this = $(this);
            var $animationDelay = $this.data('delay');
            var $animationType = 'animated ' + $this.data('animation');
            $this.css({
                'animation-delay': $animationDelay,
                '-webkit-animation-delay': $animationDelay
            });
            $this.addClass($animationType).one(animationEndEvents, function() {
                $this.removeClass($animationType);
            });
        });
    }

    $('.slide-overlay').on("animationend", function() {
        $(this).css('display', 'none');
    });

    // Check if element is scrolled into view
    function isScrolledIntoView(elem) {
        var docViewTop = $(window).scrollTop();
        var docViewBottom = docViewTop + $(window).height();

        var elemTop = $(elem).offset().top;
        var elemBottom = elemTop + $(elem).height();

        return ((elemBottom <= docViewBottom) && (elemTop + 150 >= docViewTop));
    }
    // If element is scrolled into view, fade it in
    $(window).scroll(function() {
        $('.scroll-animations .animated').each(function() {
            var $this = $(this);
            if (isScrolledIntoView(this) === true) {
                $(this).addClass('fadeInUpBig');
                $this.css({
                    'visibility': 'visible',
                    'animation-duration': '0.8s',
                    '-webkit-animation-duration': '0.8s'
                });
            }
        });
    });

    $(window).scroll(function() {
        $('.scroll-animations-image .animated').each(function() {
            var $this = $(this);
            if (isScrolledIntoView(this) === true) {
                $(this).addClass('fadeInRight');
                $this.css({
                    'visibility': 'visible',
                    'animation-duration': '0.8s',
                    '-webkit-animation-duration': '0.8s'
                });
            }
        });
    });

    var $videoSrc = $("#video").attr('src');

    // when the modal is opened autoplay it
    $('#VideoModal').on('shown.bs.modal', function(e) {
        // set the video src to autoplay and not to show related video. 
        $("#video").attr('src', $videoSrc + "?autoplay=1&amp;modestbranding=1&amp;showinfo=0&amp;loop=1&amp;playlist=pe9vEPMagWQ");
    })

    // stop playing the youtube video when modal close
    $('#VideoModal').on('hide.bs.modal', function(e) {
        $("#video").attr('src', $videoSrc);
    })

    $('a[href^="#"]').click(function() {
        var target = $(this.hash);
        if (target.length == 0) target = $('a[name="' + this.hash.substr(1) + '"]');
        if (target.length == 0) target = $('html');
        $('html, body').animate({ scrollTop: target.offset().top }, 700);
        return false;
    });

    $('#EstateModal').on('shown.bs.modal', function() {
        $('button#view-site-location-btn"').trigger('focus')
    })

    // gallery slick function

    $('.gallery-slider').slick({
        nextArrow: '<button class="slick-prev slick-arrow slick-disabled" aria-label="Previous" type="button" aria-disabled="true"><i class="fa fa-chevron-right"></i></button>',
        prevArrow: '<button class="slick-next slick-arrow" aria-label="Next" type="button" aria-disabled="false"><i class="fa fa-chevron-left"></i></button>',
        lazyLoad: 'ondemand',
        slidesToShow: 1,
        dots: false,
        arrows: true,
        fade: true,
        autoplay: false,
        autoplaySpeed: 3000,
        infinite: false,
        responsive: [{
                breakpoint: 1024,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1,
                    infinite: true,
                    dots: false
                }
            },
            {
                breakpoint: 767,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1
                }
            },
            {
                breakpoint: 480,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1
                }
            }
        ]
    });
});



function validateContactForm() {
    var valid = true;

    $(".info").html("");
    $(".input-field").css('border', '#e0dfdf 1px solid');
    var firstName = $("#firstName").val();
    var lastName = $("#lastName").val();
    var userEmail = $("#userEmail").val();
    var telNumber = $("#telNumber").val();
    var message = $("#message").val();

    if (firstName == "") {
        $("#firstName-info").html("*Required.");
        $("#firstName").css('border', '#e66262 1px solid');
        valid = false;
    }
    if (lastName == "") {
        $("#lastName-info").html("*Required.");
        $("#lastName").css('border', '#e66262 1px solid');
        valid = false;
    }
    if (userEmail == "") {
        $("#userEmail-info").html("*Required.");
        $("#userEmail").css('border', '#e66262 1px solid');
        valid = false;
    }
    if (!userEmail.match(/^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/)) {
        $("#userEmail-info").html("Invalid Email Address.");
        $("#userEmail").css('border', '#e66262 1px solid');
        valid = false;
    }
    if (telNumber == "") {
        $("#telNumber-info").html("*Required.");
        $("#telNumber").css('border', '#e66262 1px solid');
        valid = false;
    }
    if (message == "") {
        $("#userMessage-info").html("*Required.");
        $("#message").css('border', '#e66262 1px solid');
        valid = false;
    }
    return valid;
}