<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="UTF-8">
	<meta name="description" content="">
	<meta name="keywords" content="">
	<meta name="author" content="Lift Airlines">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<meta property="og:url" content="">
	<meta property="og:title" content="">
	<meta property="og:description" content="">
	<meta property="og:image" content="">
	<meta name="msapplication-config" content="images/favicons/browserconfig.xml" />
	<meta name="msapplication-TileColor" content="#ffffff">
	<meta name="msapplication-TileImage" content="images/favicons/ms-icon-144x144.png">
	<meta name="theme-color" content="#ffffff">

	<link rel="shortcut icon" href="images/favicon.png">

	<title>Lift Airline</title>

	<link href="https://fonts.googleapis.com/css?family=Josefin+Sans:300,300i,400,400i,500,500i,600,600i,700,700i&display=swap" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css2?family=Montserrat:ital,wght@0,300;0,400;0,500;0,600;0,700;1,300;1,400;1,500;1,600;1,700&display=swap" rel="stylesheet">

	<link rel="stylesheet" type="text/css" href="/css/bootstrap.min.css">
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" />
    <link rel="stylesheet" type="text/css" href="css/style.css"/>
    
    </head>

    <style>
        img {
            display:block; 
            margin: 0 auto;
        }
        .container-inner {
            background: #FFF;
            position: absolute;
            width: auto;
            padding: 1rem;
            top: 30%;
            left: 60%;
            z-index: 10;
            border: solid 2px #E1E1E1;
            border-radius: 3px;
            box-shadow: 0px 5px 15px 0px rgba(240,240,240,0.5);
        }

        a .container-inner  {
            display: block;
            font-family: 'Montserrat', sans-serif;
            font-size: 20px;
            line-height: 32px; 
            font-weight: bold;
            color: #272727; 
            text-transform: uppercase; 
            text-decoration: none;
        }

    </style>

<body>
<section id="content-main-section">
    <div class="container">
        <div>
            <img src="https://lift.co.za/images/img-lift-airline-wt.jpg" alt="Lift Airline" title="Lift Airline" />
        </div>
        
        <a href="https://ibe.aerocrs.com/">
            <div class="container-inner">click here</div>
        </a>
        
    </div>
</section>
    
</body>

</html>