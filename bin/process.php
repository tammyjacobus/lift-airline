<?php

    $firstname = isset($_POST['firstName']) ? $_POST['firstName'] : '';
    $lastname = isset($_POST['lastName']) ? $_POST['lastName'] : '';
    $email = isset($_POST['userEmail']) ? $_POST['userEmail'] : '';

    $ok = true;
    $messages = array();

    $headers = "MIME-Version: 1.0"."\r\n";
    $headers .= "Content-type:text/html;charset=UTF-8"."\r\n";
    $headers .= 'From: <website@lift.co.za>'."\r\n";

    $to = "ryan.roberts@creativespark.co.za";
    $subject = "Lift website form submission";
    $content = "<html>";
    $content .= "<head>";
    $content .= "<title>Lift website form submission</title>";
    $content .= "</head>";
    $content .= "<body>";
    $content .= "<h1>Form submitted on Lift website</h1>";
    $content .= "<p>The information submitted is as follows</p>";
    $content .= "<table>";
    $content .= "<tr><td><b>Sender: </b></td><td>".$firstname." ".$lastname."</td></tr>";
    $content .= "<tr><td><b>Email: </b></td><td>".$email."</td></tr>";
    $content .= "</table>";
    $content .= "</body>";
    $content .= "</html>";

    mail ($to,$subject,$content,$headers);