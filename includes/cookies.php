<div class="modal-container">
	<div class="modal fade" id="cookieModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
		<div class="modal-body">
			<div class="align-items-center">
			<h1 class="cookieTitle">Cookie Settings</h1>
			<div class="cookie-text">
				We use cookies so that we can offer you the best possible website experience. 
				This includes cookies which are necessary for the operation of the website and to manage our corporate commercial objectives, 
				as well as other cookies which are used solely for anonymous statistical purposes, for more comfortable website settings, 
				or for the display of personalised content. You are free to decide which categories you would like to permit. 
				Please note that depending on the settings you choose, the full functionality of the website may no longer be available.</div>
			</div>
			<div class="buttons d-flex flex-column flex-lg-row">
				<a href="#a" class="btn btn-success btn-sm" data-dismiss="modal">Accept</a>
				<a href="#a" class="btn btn-secondary btn-sm" data-dismiss="modal">Decline</a>
			</div>
		</div>
		</div>
	</div>
	</div>
</div>