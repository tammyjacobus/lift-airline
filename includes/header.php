<?php
if(!empty($_POST["send"])) {
	$firstName = $_POST["firstName"];
	$lastName = $_POST["lastName"];
	$subject = $_POST["subject"];
	$userEmail = $_POST["userEmail"];
	$telNumber = $_POST["telNumber"];
	$message = $_POST["message"];

	$ok = true;
    $messages = array();
 
    $mailHeaders = "MIME-Version: 1.0"."\r\n";
    $mailHeaders .= "Content-type:text/html;charset=UTF-8"."\r\n";
    $mailHeaders .= "From: " . $firstName . " ". $lastName ."<". $userEmail .">\r\n";
 
	$toEmail = "info@lift.co.za";
	
    $content = "<html>";
    $content .= "<head>";
    $content .= "<title>New Enquiry from Lift Airlines</title>";
    $content .= "</head>";
    $content .= "<body>";
    $content .= "<h2>You have a New Enquiry from $firstName $lastName on Lift Airlines</h2>";
    $content .= "<h3>Please find the contact details and message below:</h3>";
    $content .= "<table>";
    $content .= "<tr><td><b>Email Address: </b></td><td>".$userEmail."</td></tr>";
    $content .= "<tr><td><b>Telephone Number: </b></td><td>".$telNumber."</td></tr>";
    $content .= "<tr><td><b>Message: </b></td><td>".$message."</td></tr>";
    $content .= "</table>";
							
	if(
		mail($toEmail, $subject, $content, $mailHeaders)) {		
	    $message = "Your contact information is received successfully.";
		$type = "success";
		Header("Location: #thankyou");
	}	
}
?>
<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="UTF-8">
	<meta name="description" content="">
	<meta name="keywords" content="">
	<meta name="author" content="Lift Airlines">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<meta property="og:url" content="">
	<meta property="og:title" content="">
	<meta property="og:description" content="">
	<meta property="og:image" content="">
	<meta name="msapplication-config" content="images/favicons/browserconfig.xml" />
	<meta name="msapplication-TileColor" content="#ffffff">
	<meta name="msapplication-TileImage" content="images/favicons/ms-icon-144x144.png">
	<meta name="theme-color" content="#ffffff">

	<link rel="shortcut icon" href="images/favicon.png">

	<title>Lift Airline</title>

	<link href="https://fonts.googleapis.com/css?family=Josefin+Sans:300,300i,400,400i,500,500i,600,600i,700,700i&display=swap" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css2?family=Montserrat:ital,wght@0,300;0,400;0,500;0,600;0,700;1,300;1,400;1,500;1,600;1,700&display=swap" rel="stylesheet">

	<link rel="stylesheet" type="text/css" href="css/components/cookies.css"/>
	<link rel="stylesheet" type="text/css" href="css/animate.css"/>
	<link rel="stylesheet" type="text/css" href="css/hover.css"/>
	<link rel="stylesheet" type="text/css" href="css/slick.css"/>
	<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" />
	<link rel="stylesheet" type="text/css" href="css/style.css"/>

	<script src="js/jquery-2.2.4.min.js"></script>

	<!--<script>
	$(document).ready(function() {  
  		$('#cookieModal').modal('show');
  	});
	</script>-->

</head>

<body>
